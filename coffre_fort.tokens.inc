<?php

use Drupal\coffre_fort\Entity\CoffreFortEntity;
use Drupal\coffre_fort\PrivateDataInterface;
use Drupal\Core\Render\BubbleableMetadata;


/**
 * Implements hook_token_info().
 */
function coffre_fort_token_info()
{
  $info['types']['coffre-fort'] = [
    'name' => t("Coffre-Fort"),
    'description' => t("Tokens for private data stored on Coffre-Fort."),
  ];

  $entites = Drupal::entityTypeManager()->getStorage('coffre_fort')->loadMultiple();
  /** @var CoffreFortEntity $coffre */
  foreach ($entites as $coffre) {
    $privates = $coffre->getPrivateDataList();
    /** @var PrivateDataInterface $data */
    foreach ($privates->getIterator() as $data) {
      $name = $coffre->label() . '@' . $coffre->label();
      $description = t("Private data @name stored on Coffre-fort : @coffre", [
        '@name' => $data->label(),
        '@coffre' => $coffre->label()
      ]);

      $info['types']['coffre-fort'][$coffre->id() . ':' . $data->getName()] = [
        'name' => $name,
        'description' => $description
      ];
      $info['tokens']['coffre-fort'][$coffre->id() . ':' . $data->getName()] = [
        'name' => $name,
        'description' => $description
      ];
    }
  }
  return $info;
}


/**
 * Implements hook_tokens().
 */
function coffre_fort_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata)
{
  $replacements = [];
  if ($type == 'coffre-fort') {
    $entites = Drupal::entityTypeManager()->getStorage('coffre_fort')->loadMultiple();
    foreach ($tokens as $name => $original) {

      [$coffre_id, $private_id] = explode(':', trim($name, '[]'));
      if (!isset($entites[$coffre_id])) {
        continue;
      }
      /**
       * @var CoffreFortEntity $coffre
       */
      $coffre = $entites[$coffre_id];
      $bubbleable_metadata->addCacheTags($coffre->getCacheTags());
      $bubbleable_metadata->addCacheContexts($coffre->getCacheContexts());
      $bubbleable_metadata->addCacheContexts(['cflocked']);
      if (!$coffre->isLocked()) {
        $bubbleable_metadata->setCacheMaxAge(0);
      }
      $privates = $coffre->getPrivateDataList();
      /**
       * @var PrivateDataInterface $data
       */
      foreach ($privates->getIterator() as $data) {
        if ($data->getName() == $private_id) {
          $replacements[$original] = $mem[$original] = $data->render($coffre->isLocked(), $bubbleable_metadata);
          break;
        }
      }
    }
  }
  return $replacements;
}
