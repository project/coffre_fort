<?php

namespace Drupal\coffre_fort\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines an private data types annotation object.
 *
 * Plugin Namespace: Plugin\CoffreFort
 *
 * @see hook_coffre_fort_private_info_alter()
 * @see plugin_api
 *
 * @Annotation
 */
class PrivateDataType extends Plugin
{

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the private data type.
   *
   * @ingroup plugin_translatable
   *
   * @var Translation
   */
  public $label;

  /**
   * A brief description of the private data type.
   *
   * This property is optional and it does not need to be declared.
   *
   * This will be shown when adding or configuring this private data type.
   *
   * @ingroup plugin_translatable
   *
   * @var Translation
   */
  public $description = '';

}
