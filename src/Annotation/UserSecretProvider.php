<?php

namespace Drupal\coffre_fort\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines an user secret provider annotation object.
 *
 * Plugin Namespace: Plugin\CoffreFort
 *
 * @see hook_coffre_fort_secret_provider_info_alter()
 * @see plugin_api
 *
 * @Annotation
 */
class UserSecretProvider extends Plugin
{

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the user secret provider.
   *
   * @ingroup plugin_translatable
   *
   * @var Translation
   */
  public $label;

  /**
   * A brief description of the user secret provider.
   *
   * This property is optional and it does not need to be declared.
   *
   * This will be shown when adding or configuring this user secret provider.
   *
   * @ingroup plugin_translatable
   *
   * @var Translation
   */
  public $description = '';

}
