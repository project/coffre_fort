<?php

namespace Drupal\coffre_fort\CacheContext;

use Drupal;
use Drupal\coffre_fort\Entity\CoffreFortEntity;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CalculatedCacheContextInterface;
use Drupal\Core\Cache\Context\UserCacheContextBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Defines the LockedCoffreFortCacheContext service, for lock state caching.
 *
 * Cache context ID: 'cflocked' (to vary by lock state).
 *
 */
class LockedCoffreFortCacheContext extends UserCacheContextBase implements CalculatedCacheContextInterface
{


  /**
   * Constructs a new LockedCoffreFortCacheContext object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager)
  {
    $coffre_fort_storage = $entity_type_manager->getStorage('coffre_fort');
    $this->coffreFortStorage = $coffre_fort_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel()
  {
    Drupal::messenger()->addMessage('Cache context by each coffre-fort lock state');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext($parameter = NULL)
  {
    $result = [];
    $entites = $this->coffreFortStorage->loadMultiple();
    /** @var CoffreFortEntity $coffre */
    foreach ($entites as $coffre) {
      $result[] = $coffre->id() . '-' . $coffre->isLocked();
    }
    return implode('-', $result);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($operation = NULL)
  {
    $cacheable_metadata = new CacheableMetadata();
    $cacheable_metadata->setCacheTags(['user:' . $this->user->id()]);
    return $cacheable_metadata;
  }
}
