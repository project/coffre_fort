<?php

namespace Drupal\coffre_fort;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Coffre Fort entities.
 */
class CoffreFortEntityListBuilder extends ConfigEntityListBuilder
{

  /**
   * {@inheritdoc}
   */
  public function buildHeader()
  {
    $header['label'] = $this->t('Coffre Fort');
    $header['id'] = $this->t('Machine name');
    $header['count'] = $this->t('Nomber of items');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity)
  {
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['count'] = $entity->count();
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity)
  {
    $operations = parent::getDefaultOperations($entity);
    // Remove destination from edit form url
    if (isset($operations['edit'])) {
      $operations['edit']['url'] = $entity->toUrl('edit-form');
    }
    if ($entity->isLocked()) {
      $operations['unlock'] = [
        'title' => $this->t('Unlock'),
        'url' => $entity->toUrl('unlock'),
        'weight' => -999,
      ];
    } else {
      $operations['relock'] = [
        'title' => $this->t('Re-lock'),
        'url' => $entity->toUrl('relock'),
        'weight' => 999,
      ];
    }
    return $operations;
  }
}
