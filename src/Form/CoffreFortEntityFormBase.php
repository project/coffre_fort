<?php

namespace Drupal\coffre_fort\Form;

use Drupal\coffre_fort\CoffreFortEntityInterface;
use Drupal\coffre_fort\UserSecretProvicersManager;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CoffreFortEntityFormBase.
 */
class CoffreFortEntityFormBase extends EntityForm
{

  /**
   * The entity being used by this form.
   *
   * @var CoffreFortEntityInterface
   */
  protected $entity;

  /**
   * The Coffre Fort entity storage.
   *
   * @var EntityStorageInterface
   */
  protected $coffreFortStorage;

  /**
   * The user secret provider manager service
   * @var \Drupal\coffre_for\UserSecretProvicersManager|UserSecretProvicersManager
   */
  protected $secretProviderManager;

  /**
   * Constructs a base class for coffre fort add and edit forms.
   *
   * @param EntityStorageInterface $coffre_fort_storage
   *   The coffre fort entity storage.
   */
  public function __construct(EntityStorageInterface $coffre_fort_storage, UserSecretProvicersManager $user_secret_provider_manager)
  {
    $this->coffreFortStorage = $coffre_fort_storage;
    $this->secretProviderManager = $user_secret_provider_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('entity_type.manager')->getStorage('coffre_fort'),
      $container->get('plugin.manager.coffre_fort.secret_provider')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state)
  {

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#default_value' => $this->entity->label(),
      '#maxlength' => 255,
      '#description' => $this->t("Label for the Coffre Fort."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#machine_name' => [
        'exists' => '\Drupal\coffre_fort\Entity\CoffreFortEntity::load'
      ],
      '#default_value' => $this->entity->id(),
      '#required' => TRUE,
    ];

    $providers_type = $this->secretProviderManager->getDefinitions();
    uasort($providers_type, function ($a, $b) {
      return Unicode::strcasecmp($a['label'], $b['label']);
    });
    $config = $this->config('coffre_fort.settings');
    $default_provider = $config->get('default_providers') ?? 'password';
    $allowed_secret_providers = $config->get('allowed_providers') ?? ['password'];
    if (($key = array_search($default_provider, $allowed_secret_providers)) !== FALSE) {
      unset($allowed_secret_providers[$key]);
    }
    array_unshift($allowed_secret_providers, $default_provider);


    if (count($allowed_secret_providers) > 1) {
      $form['user_secret'] = [
        '#type' => $this->entity->isNew() ? 'fieldset' : 'details',
        '#title' => 'Coffre-fort secret',
        '#visible' => TRUE,
        '#collapsible' => !$this->entity->isNew(),
        '#collapsed' => !$this->entity->isNew(),
        '#description' => $this->t('You need to configure the secret encryption provider.'),
        '#attributes' => ['style' => ['width: 950px']],
      ];

      $form['user_secret']['secret_provider'] = [
        '#type' => 'select',
        '#label' => t('Select secret provider'),
        '#attributes' => ['style' => ['width: 900px']],
        '#default_option' => $default_provider,
        '#options' => [],
        '#tree' => false
      ];

    } else {
      $form['user_secret'] = [];
      $form['user_secret']['secret_provider'] = [
        '#type' => 'value',
        '#value' => $default_provider,
        '#tree' => false
      ];
    }

    foreach ($allowed_secret_providers as $provider_id) {
      $form['user_secret']['secret_provider']['#options'][$provider_id] = $providers_type[$provider_id]['label'];
      $form['user_secret'][$provider_id] = [];
      $provider_instance = $this->secretProviderManager->createInstance($provider_id);
      $subform_state = SubformState::createForSubform($form['user_secret'][$provider_id], $form, $form_state);
      $subform_state->set('entity', $this->entity);
      $form['user_secret'][$provider_id] = $provider_instance->buildConfigurationForm($form['user_secret'][$provider_id], $subform_state);
      $form['user_secret'][$provider_id]['#tree'] = TRUE;
    }
    return parent::form($form, $form_state);
  }


  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    $provider_id = $form_state->getValue('secret_provider');
    if (!$this->secretProviderManager->hasDefinition($provider_id)) {
      $form_state->setErrorByName($form['secret_provider'], $this->t('missing provider @id', ['@id' => $provider_id]));
    }

    $provider_instance = $this->secretProviderManager->createInstance($provider_id);
    $subform_state = SubformState::createForSubform($form['user_secret'][$provider_id], $form, $form_state);
    $provider_instance->validateConfigurationForm($form['user_secret'][$provider_id], $subform_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state)
  {
    $config = $this->config('coffre_fort.settings');
    $default_provider = $config->get('default_providers') ?? 'password';
    $provider_id = $form_state->getValue('secret_provider', $default_provider);
    $form_state->set('entity', $this->entity);
    $provider_instance = $this->secretProviderManager->createInstance($provider_id);
    $subform_state = SubformState::createForSubform($form['user_secret'][$provider_id], $form, $form_state);
    $subform_state->set('entity', $this->entity);
    $provider_instance->submitConfigurationForm($form['user_secret'][$provider_id], $subform_state);
    parent::save($form, $form_state);
    $form_state->setRedirectUrl($this->entity->toUrl('edit-form'));

  }

}
