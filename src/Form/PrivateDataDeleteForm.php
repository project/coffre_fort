<?php

declare(strict_types=1);

namespace Drupal\coffre_fort\Form;

use Drupal\coffre_fort\CoffreFortEntityInterface;
use Drupal\coffre_fort\PrivateDataInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form to delete a group on a channel.
 *
 * @package Drupal\coffre_fort\Form
 */
class PrivateDataDeleteForm extends ConfirmFormBase
{

  /**
   * The coffre-fort entity.
   *
   * @var CoffreFortEntityInterface
   */
  protected $coffreFort;

  /**
   * The private data to be deleted.
   *
   * @var PrivateDataInterface
   */
  protected $privateData;

  /**
   * {@inheritdoc}
   */
  public function getQuestion()
  {
    return $this->t('Are you sure you want to delete the @private private data from the coffre-fort %coffre ?', ['%coffre' => $this->coffreFort->label(), '@private' => $this->privateData->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText()
  {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl()
  {
    return $this->coffreFort->toUrl('edit-form');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'private_data_delete_form';
  }


  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param FormStateInterface $form_state
   *   The current state of the form.
   * @param CoffreFortEntityInterface $coffre_fort
   *   The coffre fort.
   * @param string $private_data_id
   *   The private data type.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, CoffreFortEntityInterface $coffre_fort = NULL, $private_data_id = NULL)
  {
    $this->coffreFort = $coffre_fort;
    $this->privateData = $coffre_fort->getPrivateData($private_data_id);
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $this->coffreFort->deletePrivateData($this->privateData);
    $this->messenger()->addStatus($this->t('The private data %name has been deleted.', ['%name' => $this->privateData->label()]));
    $form_state->setRedirectUrl($this->coffreFort->toUrl('edit-form'));
  }

}
