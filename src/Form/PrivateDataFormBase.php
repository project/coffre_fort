<?php


namespace Drupal\coffre_fort\Form;

use Drupal\coffre_fort\CoffreFortEntityInterface;
use Drupal\coffre_fort\ConfigurablePrivateDataInterface;
use Drupal\coffre_fort\PrivateDataInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Base class for private data  form.
 *
 * @package Drupal\coffre_fort\Form
 */
abstract class PrivateDataFormBase extends FormBase
{

  /**
   * The coffre fort entity.
   *
   * @var CoffreFortEntityInterface
   */
  protected $coffreFort;

  /**
   * The private data instance.
   *
   * @var PrivateDataInterface |ConfigurablePrivateDataInterface
   */
  protected $privateData;

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'private_data_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param FormStateInterface $form_state
   *   The current state of the form.
   * @param CoffreFortEntityInterface $coffre_fort
   *   The coffre fort.
   * @param string $private_data_type
   *   The private data type.
   *
   * @return array
   *   The form structure.
   *
   * @throws NotFoundHttpException
   */
  public function buildForm(array $form, FormStateInterface $form_state, CoffreFortEntityInterface $coffre_fort = NULL, $private_data_type = NULL)
  {
    $this->coffreFort = $coffre_fort;
    try {
      $this->privateData = $this->preparePrivateData($private_data_type);
    } catch (PluginNotFoundException $e) {
      throw new NotFoundHttpException("Invalid private data id: '.$private_data_type'.");
    }
    $request = $this->getRequest();

    if (!($this->privateData instanceof ConfigurablePrivateDataInterface)) {
      throw new NotFoundHttpException();
    }

    $form['#attached']['library'][] = 'coffre_fort/admin';

    $form['uuid'] = [
      '#type' => 'value',
      '#value' => $this->privateData->getUuid(),
    ];

    $form['id'] = [
      '#type' => 'value',
      '#value' => $this->privateData->getPluginId(),
    ];

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => t('label'),
      '#default_value' => $this->privateData->label(),
      '#required' => TRUE,
    ];

    $form['name'] = [
      '#type' => 'machine_name',
      '#machine_name' => [
        'exists' => [$this->coffreFort, 'privateDataNameExists'],
      ],
      '#required' => TRUE,
    ];

    $form['data'] = [];
    $subform_state = SubformState::createForSubform($form['data'], $form, $form_state);
    $form['data'] = $this->privateData->buildConfigurationForm($form['data'], $subform_state);
    $form['data']['#tree'] = TRUE;

    $form['weight'] = [
      '#type' => 'hidden',
      '#value' => $request->query->has('weight') ? (int)$request->query->get('weight') : $this->privateData->getWeight(),
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
    ];
    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => $this->coffreFort->toUrl('edit-form'),
      '#attributes' => ['class' => ['button']],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    $this->privateData->validateConfigurationForm($form['data'], SubformState::createForSubform($form['data'], $form, $form_state));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $form_state->cleanValues();
    $this->privateData->submitConfigurationForm($form['data'], SubformState::createForSubform($form['data'], $form, $form_state));
    $this->privateData->setLabel($form_state->getValue('label'));
    $this->privateData->setName($form_state->getValue('name'));
    $this->privateData->setWeight($form_state->getValue('weight'));

    if (!$this->privateData->getUuid()) {
      $this->coffreFort->addPrivateData($this->privateData->getConfiguration());
    }

    $this->coffreFort->save();
    $this->messenger()->addStatus($this->t('The private data was successfully added.'));
    $form_state->setRedirectUrl($this->coffreFort->toUrl('edit-form'));

  }

  /**
   * Converts an private data ID into an object.
   */
  abstract protected function preparePrivateData($private_data);

}
