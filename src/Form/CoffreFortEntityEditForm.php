<?php

namespace Drupal\coffre_fort\Form;


use Drupal;
use Drupal\coffre_fort\ConfigurablePrivateDataInterface;
use Drupal\coffre_fort\PrivateDataInterface;
use Drupal\coffre_fort\PrivateDataManager;
use Drupal\coffre_fort\UserSecretProvicersManager;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for coffre fort addition forms.
 *
 * @internal
 */
class CoffreFortEntityEditForm extends CoffreFortEntityFormBase
{

  /**
   * The Private Data manager service.
   *
   * @var PrivateDataManager
   */
  protected $privateDataManager;


  /**
   * Constructs an CoffreFortEntityEditForm object.
   *
   * @param EntityStorageInterface $coffre_fort_storage
   *   The storage.
   * @param Drupal\coffre_for\PrivateDataManager $private_data_manager
   *   The private data manager service.
   * @param Drupal\coffre_for\UserSecretProvicersManager $user_secret_provider_manager
   *   The user secret provider manager service.
   */
  public function __construct(EntityStorageInterface $coffre_fort_storage, UserSecretProvicersManager $user_secret_provider_manager, PrivateDataManager $private_data_manager)
  {
    parent::__construct($coffre_fort_storage, $user_secret_provider_manager);
    $this->privateDataManager = $private_data_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('entity_type.manager')->getStorage('coffre_fort'),
      $container->get('plugin.manager.coffre_fort.secret_provider'),
      $container->get('plugin.manager.coffre_fort.private')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state)
  {

    $user_input = $form_state->getUserInput();

    $form['#title'] = $this->t('Update Coffre-fort : %name ', ['%name' => $this->entity->label()]);
    $form['#tree'] = TRUE;
    $form['#attached']['library'][] = 'coffre_fort/admin';

    // Build the list of existing private data.

    $form['private_data'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Label'),
        $this->t('Replacement Token'),
        $this->t('Weight'),
        $this->t('Operations'),
      ],
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'private-data-order-weight',
        ],
      ],
      '#attributes' => [
        'id' => 'coffre-fort-private-data',
      ],
      '#empty' => $this->t('There are currently no private data in this coffre-fort. Add one by selecting an option below.'),
      '#weight' => 5,
    ];
    /** @var PrivateDataInterface $privateData */
    foreach ($this->entity->getPrivateDataList() as $privateData) {
      $key = $privateData->getUuid();
      $form['private_data'][$key]['#attributes']['class'][] = 'draggable';
      $form['private_data'][$key]['#weight'] = isset($user_input['private_data']) ? $user_input['private_data'][$key]['weight'] : NULL;
      $form['private_data'][$key]['private'] = [
        '#tree' => FALSE,
        'data' => [
          'label' => [
            '#plain_text' => $privateData->label() . ' ( ' . $privateData->getPluginId() . ' )',
          ],
        ],
      ];

      $form['private_data'][$key]['token'] = [
        '#tree' => FALSE,
        'data' => [
          'label' => [
            '#plain_text' => '[coffre-fort:' . $this->getEntity()->id() . ':' . $privateData->getName() . ']',
          ],
        ],
      ];

      $form['private_data'][$key]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for @title', ['@title' => $privateData->label()]),
        '#title_display' => 'invisible',
        '#default_value' => $privateData->getWeight(),
        '#attributes' => [
          'class' => ['private-data-order-weight']
        ],
      ];

      $links = [];
      $is_configurable = $privateData instanceof ConfigurablePrivateDataInterface;
      if ($is_configurable) {
        $links['edit'] = [
          'title' => $this->t('Edit'),
          'url' => Url::fromRoute('coffre_fort.private_edit_form', [
            'coffre_fort' => $this->entity->id(),
            'private_data_id' => $key,
          ]),
        ];
      }
      $links['delete'] = [
        'title' => $this->t('Delete'),
        'url' => Url::fromRoute('coffre_fort.private_delete', [
          'coffre_fort' => $this->entity->id(),
          'private_data_id' => $key,
        ]),
      ];
      $form['private_data'][$key]['operations'] = [
        '#type' => 'operations',
        '#links' => $links,
      ];
    }

    // Build the new private data addition form and add it to the data list.
    $new_private_data_options = [];
    $private_types = $this->privateDataManager->getDefinitions();
    uasort($private_types, function ($a, $b) {
      return Unicode::strcasecmp($a['label'], $b['label']);
    });

    foreach ($private_types as $type => $definition) {
      $new_private_data_options[$type] = $definition['label'];
    }

    $form['private_data']['new'] = [
      '#tree' => FALSE,
      '#weight' => $user_input['weight'] ?? NULL,
      '#attributes' => ['class' => ['draggable']],
    ];
    $form['private_data']['new']['private'] = [
      'data' => [
        'new' => [
          '#type' => 'select',
          '#title' => $this->t('Type'),
          '#title_display' => 'invisible',
          '#options' => $new_private_data_options,
          '#empty_option' => $this->t('Select private type'),
        ],
        [
          'add' => [
            '#type' => 'submit',
            '#value' => $this->t('Add'),
            '#validate' => ['::privateValidate'],
            '#submit' => ['::submitForm', '::privateDataSave'],
          ],
        ],
      ],
      '#prefix' => '<div class="private-data-new">',
      '#suffix' => '</div>',
    ];
    $form['private_data']['new']['token'] = [];
    $form['private_data']['new']['weight'] = [
      '#type' => 'weight',
      '#title' => $this->t('Weight for new private'),
      '#title_display' => 'invisible',
      '#default_value' => count($this->entity->getPrivateDataList()) + 1,
      '#attributes' => ['class' => ['private-data-order-weight']],
    ];
    $form['private_data']['new']['operations'] = [
      'data' => [],
    ];

    return parent::form($form, $form_state);
  }

  /**
   * Validate data types
   *
   * @param $form
   * @param FormStateInterface $form_state
   * @return void
   */
  public function privateValidate($form, FormStateInterface $form_state)
  {
    if (!$form_state->getValue('new')) {
      $form_state->setErrorByName('new', $this->t('Select a type for private data to add.'));
    }
  }

  /**
   * Submit handler for new private.
   */
  public function privateDataSave($form, FormStateInterface $form_state)
  {
    $this->save($form, $form_state);

    // Check if this field has any configuration options.
    $privateDataType = $this->privateDataManager->getDefinition($form_state->getValue('new'));

    // Load the configuration form for this option.
    if (is_subclass_of($privateDataType['class'], '\Drupal\coffre_fort\ConfigurablePrivateDataInterface')) {
      Drupal::request()->query->remove('destination');
      $form_state->setRedirect(
        'coffre_fort.private_add_form',
        [
          'coffre_fort' => $this->entity->id(),
          'private_data_type' => $form_state->getValue('new'),
        ],
        ['query' => ['weight' => $form_state->getValue('weight')]]
      );
    } // If there's no form, immediately add the private data.
    else {
      $privateDataType = [
        'id' => $privateDataType['id'],
        'data' => [],
        'weight' => $form_state->getValue('weight'),
      ];
      $data_id = $this->entity->addPrivateData($privateDataType);
      $this->entity->save();
      if (!empty($data_id)) {
        $this->messenger()->addStatus($this->t('The private data was successfully added.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {

    if (!$form_state->isValueEmpty('private_data')) {
      $this->updatePrivateDataWeights($form_state->getValue('private_data'));
    }

    parent::submitForm($form, $form_state);
  }

  protected function updatePrivateDataWeights(array $privateData)
  {
    foreach ($privateData as $uuid => $secret) {
      if ($this->entity->getPrivateDataList()->has($uuid)) {
        $this->entity->getPrivateData($uuid)->setWeight($secret['weight']);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state)
  {
    parent::save($form, $form_state);
    $this->messenger()->addStatus($this->t('Coffre-fort updated.'));
  }

  /**
   * {@inheritdoc}
   */
  public function actions(array $form, FormStateInterface $form_state)
  {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Update Coffre-fort');

    $route_info = $this->entity->toUrl('relock');
    if ($this->getRequest()->query->has('destination')) {
      $query = $route_info->getOption('query');
      $query['destination'] = $this->getRequest()->query->get('destination');
      $route_info->setOption('query', $query);
    }
    $actions['relock'] = [
      '#type' => 'link',
      '#url' => $route_info,
      '#title' => $this->t('Re-lock'),
      '#access' => $this->entity->access('relock'),
      '#attributes' => [
        'class' => ['button'],
      ],
    ];
    return $actions;
  }

}
