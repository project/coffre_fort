<?php

namespace Drupal\coffre_fort\Form;


use Drupal\coffre_fort\CoffreFortEntityInterface;
use Drupal\coffre_fort\UserSecretProvicersManager;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CoffreFortEntityUnlockForm.
 */
class CoffreFortEntityLockForm extends EntityForm
{

  /**
   * The entity being used by this form.
   *
   * @var CoffreFortEntityInterface
   */
  protected $entity;

  /**
   * The Coffre Fort entity storage.
   *
   * @var EntityStorageInterface
   */
  protected $coffreFortStorage;

  /**
   * The user secret provider manager service
   * @var \Drupal\coffre_for\UserSecretProvicersManager|UserSecretProvicersManager
   */
  protected $secretProviderManager;

  /**
   * Constructs a base class for coffre fort add and edit forms.
   *
   * @param EntityStorageInterface $coffre_fort_storage
   *   The coffre fort entity storage.
   */
  public function __construct(EntityStorageInterface $coffre_fort_storage, UserSecretProvicersManager $user_secret_provider_manager)
  {
    $this->coffreFortStorage = $coffre_fort_storage;
    $this->secretProviderManager = $user_secret_provider_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('entity_type.manager')->getStorage('coffre_fort'),
      $container->get('plugin.manager.coffre_fort.secret_provider')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state)
  {

    $provider_id = $this->entity->get('secret_provider');
    $form['user_secret'][$provider_id] = [];
    $provider_instance = $this->secretProviderManager->createInstance($provider_id);
    $subform_state = SubformState::createForSubform($form['user_secret'][$provider_id], $form, $form_state);
    $subform_state->set('entity', $this->entity);
    $form['user_secret'][$provider_id] = $provider_instance->buildlockForm($form['user_secret'][$provider_id], $subform_state);
    $form['user_secret'][$provider_id]['#tree'] = TRUE;

    return parent::form($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state)
  {
    return [
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Re-Lock'),
        '#submit' => [
          [$this, 'submitForm'],
        ],
      ],
      'cancel' => [
        '#type' => 'link',
        '#title' => $this->t('Cancel'),
        '#attributes' => ['class' => ['button']],
        '#url' => $this->entity->toUrl('collection'),
        '#cache' => [
          'contexts' => [
            'url.query_args:destination',
          ],
        ],
      ]
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    $provider_id = $this->entity->get('secret_provider');
    if (!$this->secretProviderManager->hasDefinition($provider_id)) {
      $form_state->setErrorByName($form['secret_provider'], $this->t('missing provider @id', ['@id' => $provider_id]));
    }
    $provider_instance = $this->secretProviderManager->createInstance($provider_id);
    $subform_state = SubformState::createForSubform($form['user_secret'][$provider_id], $form, $form_state);
    $provider_instance->validatelockForm($form['user_secret'][$provider_id], $subform_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $provider_id = $this->entity->get('secret_provider');
    $form_state->set('entity', $this->entity);
    $provider_instance = $this->secretProviderManager->createInstance($provider_id);
    $subform_state = SubformState::createForSubform($form['user_secret'][$provider_id], $form, $form_state);
    $subform_state->set('entity', $this->entity);
    $provider_instance->submitlockForm($form['user_secret'][$provider_id], $subform_state);
    $this->messenger()->addStatus($this->t('The coffre fort unlock expired, you can unlock any time .'));
    $form_state->setRedirectUrl($this->entity->toUrl('edit-form'));
  }

}
