<?php

namespace Drupal\coffre_fort\Form;

use Drupal\coffre_fort\CoffreFortEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Form to edit a private data on a Coffre fort.
 *
 * @package Drupal\coffre_fort\Form
 */
class PrivateDataEditForm extends PrivateDataFormBase
{


  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param FormStateInterface $form_state
   *   The current state of the form.
   * @param CoffreFortEntityInterface $coffre_fort
   *   The coffre fort.
   * @param string $private_data_id
   *   The private data type.
   *
   * @return array
   *   The form structure.
   *
   * @throws NotFoundHttpException
   */
  public function buildForm(array $form, FormStateInterface $form_state, CoffreFortEntityInterface $coffre_fort = NULL, $private_data_id = NULL)
  {
    $form = parent::buildForm($form, $form_state, $coffre_fort, $private_data_id);

    $form['#title'] = $this->t('Edit %label private data', ['%label' => $this->privateData->label()]);
    $form['actions']['submit']['#value'] = $this->t('Update private data');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function preparePrivateData($private_data_id)
  {
    return $this->coffreFort->getPrivateData($private_data_id);
  }


}
