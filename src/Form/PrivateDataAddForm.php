<?php

declare(strict_types=1);

namespace Drupal\coffre_fort\Form;

use Drupal\coffre_fort\CoffreFortEntityInterface;
use Drupal\coffre_fort\PrivateDataManager;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Form to add a group on a channel.
 *
 * @package Drupal\coffre_fort\Form
 */
class PrivateDataAddForm extends PrivateDataFormBase
{

  /**
   * The private data manager.
   *
   * @var PrivateDataManager
   *
   */
  protected $privateDataManager;

  /**
   * Constructs a new PrivateDataAddForm
   * @param PrivateDataManager $private_data_manager
   */
  public function __construct(PrivateDataManager $private_data_manager)
  {
    $this->privateDataManager = $private_data_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('plugin.manager.coffre_fort.private')
    );
  }


  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param FormStateInterface $form_state
   *   The current state of the form.
   * @param CoffreFortEntityInterface $coffre_fort
   *   The coffre fort.
   * @param string $private_data_type
   *   The private data type.
   *
   * @return array
   *   The form structure.
   *
   * @throws NotFoundHttpException
   */
  public function buildForm(array $form, FormStateInterface $form_state, CoffreFortEntityInterface $coffre_fort = NULL, $private_data_type = NULL)
  {
    $form = parent::buildForm($form, $form_state, $coffre_fort, $private_data_type);

    $form['#title'] = $this->t('Add %label private data', ['%label' => $this->privateData->label()]);
    $form['actions']['submit']['#value'] = $this->t('Add private data');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function preparePrivateData($private_data_type)
  {
    $private_data = $this->privateDataManager->createInstance($private_data_type);
    $private_data->setWeight(count($this->coffreFort->getPrivateDataList()));
    return $private_data;
  }
}
