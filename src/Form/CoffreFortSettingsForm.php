<?php

namespace Drupal\coffre_fort\Form;

use Drupal\coffre_fort\UserSecretProvicersManager;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CoffreFortSettingsForm.
 *
 * @package Drupal\coffre_fort\Form
 */
class CoffreFortSettingsForm extends ConfigFormBase
{
  /**
   * The user secret provider manager service
   * @var \Drupal\coffre_for\UserSecretProvicersManager|UserSecretProvicersManager
   */
  protected $secretProviderManager;

  /**
   * Constructs an CoffreFortSettingsForm object.
   *
   * @param \Drupal\coffre_for\UserSecretProvicersManager $user_secret_provider_manager
   *   The user secret provider manager service.
   */
  public function __construct(UserSecretProvicersManager $user_secret_provider_manager)
  {

    $this->secretProviderManager = $user_secret_provider_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('plugin.manager.coffre_fort.secret_provider')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'coffre_fort.settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return ['coffre_fort.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $config = $this->config('coffre_fort.settings');
    $form = [];

    $form['settings'] = [
      '#type' => 'fieldset',
      '#title' => t('Settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('.'),
    ];

    $list_plugins = [];
    $secret_provider = $this->secretProviderManager->getDefinitions();
    uasort($secret_provider, function ($a, $b) {
      return Unicode::strcasecmp($a['label'], $b['label']);
    });
    foreach ($secret_provider as $type => $definition) {
      $list_plugins[$type] = $definition['label'];
    }


    $form['settings']['default_providers'] = [
      '#type' => 'select',
      '#title' => t('Default Secret prividers'),
      '#required' => TRUE,
      '#default_value' => $config->get('default_providers') ?? 'password',
      '#options' => $list_plugins,
      '#description' => t('You should select the default secret privders available for users.')
    ];

    $form['settings']['allowed_providers'] = [
      '#type' => 'checkboxes',
      '#title' => t('Enabled Secret prividers'),
      '#required' => TRUE,
      '#default_value' => $config->get('allowed_providers') ?? ['password'],
      '#options' => $list_plugins,
      '#description' => t('You should enable secret privders available for users.')
    ];

    $form['settings']['exclude_config'] = [
      '#type' => 'checkbox',
      '#title' => t('Exclude Coffre fort configs from import or export'),
      '#default_value' => $config->get('exclude_config') ?? TRUE,
      '#description' => t('Keep this option checked for better security.')
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $config = $this->config('coffre_fort.settings');
    $config->set('default_providers', $form_state->getValue('default_providers', 'password'));
    $config->set('exclude_config', $form_state->getValue('exclude_config', TRUE));
    $allowed = $form_state->getValue('allowed_providers', ['password']);
    $allowed = array_filter($allowed);
    $config->set('allowed_providers', $allowed);
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
