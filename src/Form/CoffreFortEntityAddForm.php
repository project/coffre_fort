<?php

namespace Drupal\coffre_fort\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Controller for coffre fort addition forms.
 *
 * @internal
 */
class CoffreFortEntityAddForm extends CoffreFortEntityFormBase
{

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    parent::submitForm($form, $form_state);
    $this->messenger()->addStatus($this->t('The Coffre-fort %name was created.', ['%name' => $this->entity->label()]));
  }

  /**
   * {@inheritdoc}
   */
  public function actions(array $form, FormStateInterface $form_state)
  {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Create new Coffre-fort');

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  protected function preparePrivateData($private_data_type)
  {
    $private_data = $this->privateDataManager->createInstance($private_data_type);
    $private_data->setWeight(count($this->coffreFort->getPrivateDataList()));
    return $private_data;
  }
}
