<?php

namespace Drupal\coffre_fort;

use Drupal\Core\Plugin\DefaultLazyPluginCollection;

/**
 * A collection of Private Data.
 */
class PrivateDataPluginCollection extends DefaultLazyPluginCollection
{

  /**
   * {@inheritdoc}
   *
   * @return PrivateDataInterface
   */
  public function &get($instance_id)
  {
    return parent::get($instance_id);
  }

  /**
   * {@inheritdoc}
   */
  public function sortHelper($aID, $bID)
  {
    return $this->get($aID)->getWeight() <=> $this->get($bID)->getWeight();
  }

}
