<?php

namespace Drupal\coffre_fort;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base class for user secret provider.
 */
abstract class UserSecretProviderBase extends PluginBase implements UserSecretProviderInterface, ContainerFactoryPluginInterface
{

  /**
   * The private data ID.
   *
   * @var string
   */
  protected $uuid;

  /**
   * The private data ID.
   *
   * @var string
   */
  protected $label;

  /**
   * The weight of the private data.
   *
   * @var int|string
   */
  protected $weight = '';

  /**
   * A logger instance.
   *
   * @var LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerInterface $logger)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setConfiguration($configuration);
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')->get('coffre_fort')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transformDimensions(array &$dimensions, $uri)
  {
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeExtension($extension)
  {
    return $extension;
  }

  /**
   * {@inheritdoc}
   */
  public function label()
  {
    return $this->configuration['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function type()
  {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getUuid()
  {
    return $this->uuid;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight)
  {
    $this->weight = $weight;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight()
  {
    return $this->weight;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration()
  {
    return [
      'uuid' => $this->getUuid(),
      'id' => $this->getPluginId(),
      'weight' => $this->getWeight(),
      'data' => $this->configuration,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration)
  {
    $configuration += [
      'data' => [],
      'uuid' => '',
      'weight' => '',
    ];
    $this->configuration = $configuration['data'] + $this->defaultConfiguration();
    $this->uuid = $configuration['uuid'];
    $this->weight = $configuration['weight'];
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration()
  {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies()
  {
    return [];
  }

  public function getSecretKey($entity)
  {
    return FALSE;
  }


  public function buildConfigurationForm(array $form, FormStateInterface $form_state)
  {

  }

  public function validateConfigurationForm(array &$form, FormStateInterface $form_state)
  {

  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state)
  {

  }

  public function buildUnlockForm(array $form, FormStateInterface $form_state)
  {

  }

  public function validateUnclockForm(array $form, FormStateInterface $form_state)
  {

  }

  public function submitUnlockForm(array $form, FormStateInterface $form_state)
  {

  }

  public function buildlockForm(array $form, FormStateInterface $form_state)
  {

  }

  public function validatelockForm(array $form, FormStateInterface $form_state)
  {

  }

  public function submitlockForm(array $form, FormStateInterface $form_state)
  {

  }

}
