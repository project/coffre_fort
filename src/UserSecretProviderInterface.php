<?php

namespace Drupal\coffre_fort;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the interface for user secret provider.
 */
interface UserSecretProviderInterface extends PluginInspectionInterface, ConfigurableInterface, DependentPluginInterface
{


  /**
   * Returns the private data label.
   *
   * @return string
   *   The private data label.
   */
  public function label();

  public function getSecretKey($entity);

  public function buildConfigurationForm(array $form, FormStateInterface $form_state);

  public function validateConfigurationForm(array &$form, FormStateInterface $form_state);

  public function submitConfigurationForm(array &$form, FormStateInterface $form_state);

  public function buildUnlockForm(array $form, FormStateInterface $form_state);

  public function validateUnclockForm(array $form, FormStateInterface $form_state);

  public function submitUnlockForm(array $form, FormStateInterface $form_state);

  public function buildlockForm(array $form, FormStateInterface $form_state);

  public function validatelockForm(array $form, FormStateInterface $form_state);

  public function submitlockForm(array $form, FormStateInterface $form_state);

}
