<?php

namespace Drupal\coffre_fort\Entity;

use Drupal;
use Drupal\coffre_fort\CoffreFortEncryption;
use Drupal\coffre_fort\CoffreFortEntityInterface;
use Drupal\coffre_fort\PrivateDataInterface;
use Drupal\coffre_fort\PrivateDataPluginCollection;
use Drupal\coffre_fort\UserSecretProvicersManager;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Exception;

/**
 * Defines the Coffre Fort entity.
 *
 * @ConfigEntityType(
 *   id = "coffre_fort",
 *   label = @Translation("Confidential store"),
 *   label_collection = @Translation("List of all confidential store"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\coffre_fort\CoffreFortEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\coffre_fort\Form\CoffreFortEntityAddForm",
 *       "edit" = "Drupal\coffre_fort\Form\CoffreFortEntityEditForm",
 *       "delete" = "Drupal\coffre_fort\Form\CoffreFortEntityDeleteForm",
 *       "unlock" = "Drupal\coffre_fort\Form\CoffreFortEntityUnlockForm",
 *       "relock" = "Drupal\coffre_fort\Form\CoffreFortEntityLockForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\coffre_fort\CoffreFortEntityHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\coffre_fort\CoffreFortAccessControlHandler"
 *   },
 *   config_prefix = "coffre_fort",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/coffre_fort/{coffre_fort}",
 *     "add-form" = "/admin/structure/coffre_fort/add",
 *     "edit-form" = "/admin/structure/coffre_fort/{coffre_fort}/edit",
 *     "delete-form" = "/admin/structure/coffre_fort/{coffre_fort}/delete",
 *     "collection" = "/admin/structure/coffre_fort",
 *     "unlock" = "/admin/structure/coffre_fort/{coffre_fort}/unlock",
 *     "relock" = "/admin/structure/coffre_fort/{coffre_fort}/relock"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "secret_provider",
 *     "secret",
 *     "private_data_list"
 *   }
 * )
 */
class CoffreFortEntity extends ConfigEntityBase implements CoffreFortEntityInterface, EntityWithPluginCollectionInterface
{

  /**
   * The Coffre Fort ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Coffre Fort label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Coffre Fort secret provider.
   *
   * @var string
   */
  protected $secret_provider;
  /**
   * The Coffre Fort encrypted secret.
   *
   * @var string
   */
  protected $secret;
  /**
   * The Coffre Fort decrypted secret.
   *
   * @var string
   */
  private $real_secret = NULL;

  /**
   * The Coffre Fort lock status.
   *
   * @var string
   */
  private $locked = TRUE;
  private $check_once = FALSE;

  /**
   * The array of private data stored on the Coffre-fort.
   *
   * @var array
   */
  protected $private_data_list = [];

  /**
   * Holds the collection of all private data stored on the Coffre-fort.
   * @var PrivateDataPluginCollection $privateDataCollection
   */
  protected $privateDataCollection;

  /**
   * Set default cache context
   * @var string[]
   */
  protected $cacheContexts = ['cflocked'];

  /**
   * {@inheritdoc}
   */
  public function id()
  {
    return $this->id;
  }

  /**
   * Returns the private data plugin manager.
   *
   * @return PluginManagerInterface
   *   The private data plugin manager.
   */
  protected function getPrivateDataPluginManager()
  {
    return Drupal::service('plugin.manager.coffre_fort.private');
  }

  /**
   * @return UserSecretProvicersManager
   */
  protected function getSecretProviderPluginManager()
  {
    return Drupal::service('plugin.manager.coffre_fort.secret_provider');
  }


  /**
   * {@inheritdoc}
   */
  public function getPrivateData($privateData)
  {
    return $this->getPrivateDataList()->get($privateData);
  }

  /**
   * {@inheritdoc}
   */
  public function getPrivateDataList()
  {
    if (!$this->privateDataCollection) {
      if (!$this->locked) {
        //decrypt all secret data ..
        foreach ($this->private_data_list as &$configuration) {
          $configuration['data']['secret'] = $this->getEncryptionService()
            ->decrypt($configuration['data']['encrypted'], $this->real_secret);
        }
      }
      $this->privateDataCollection = new PrivateDataPluginCollection(
        $this->getPrivateDataPluginManager(),
        $this->private_data_list
      );
      $this->privateDataCollection->sort();
    }
    return $this->privateDataCollection;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections()
  {
    $list = clone $this->getPrivateDataList();
    return ['private_data_list' => $this->getPrivateDataList()];
  }

  /**
   * {@inheritdoc}
   */
  public function addPrivateData(array $configuration)
  {
    if ($this->locked) {
      throw new Exception('Trying to update data for locked coffre-fort');
    }
    $configuration['uuid'] = $this->uuidGenerator()->generate();
    $this->getPrivateDataList()->addInstanceId($configuration['uuid'], $configuration);
    return $configuration['uuid'];
  }


  public function deletePrivateData(PrivateDataInterface $private_data)
  {
    $this->getPrivateDataList()->removeInstanceId($private_data->getUuid());
    $this->save();
    return $this;
  }

  /**
   * expose encrypt function that can be used by datatypes plugins to encrypt data
   * @param $string
   * @return void
   */
  public function encrypt($string)
  {
    return $this->getEncryptionService()->encrypt($string, $this->real_secret);
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel()
  {
    return $this->get('label');
  }

  /**
   * {@inheritdoc}
   */
  public function setLabel($name)
  {
    $this->set('label', $name);
    return $this;
  }

  /**
   * Returns the coffre fort encryption service
   *
   * @return CoffreFortEncryption
   *
   */
  protected function getEncryptionService()
  {
    return Drupal::service('coffre_fort.encryption');
  }

  /**
   * {@inheritdoc}
   */
  public function getSecret($secret_key)
  {
    static $raw_secret;

    if (empty($raw_secret)) {
      $encrypted_secret = $this->get('secret');
      $raw_secret = $this->getEncryptionService()->decryptSecret($encrypted_secret, $secret_key);
    }
    return $raw_secret;
  }

  /**
   * try to unlock using provider session
   * @return void
   * @throws Drupal\Component\Plugin\Exception\PluginException
   */
  protected function tryUnlockByProvider()
  {
    if (!$this->check_once) {
      $this->check_once = TRUE;
      if (!$this->isNew()) {
        $service = $this->getSecretProviderPluginManager();
        if ($service->hasDefinition($this->secret_provider)) {
          $provider_instance = $service->createInstance($this->secret_provider);
          if ($secret_key = $provider_instance->getSecretKey($this)) {
            $this->real_secret = $secret_key;
            $this->locked = FALSE;
            if ($this->privateDataCollection) {
              $this->privateDataCollection = NULL;
            }
          }
        }
      }
    }
  }

  /**
   * check if Coffre-fort is locked
   * @return bool
   */
  public function isLocked()
  {
    $this->tryUnlockByProvider();
    return TRUE === $this->locked;
  }

  /**
   * Try to unlock using provided secret key.
   * @param $secret_key
   * @param $dry_run
   * @return bool
   */
  public function unlock($secret_key, $dry_run = false)
  {
    $encryption_service = $this->getEncryptionService();
    $raw_secret = $this->get('secret');
    try {
      $real_secret = $encryption_service->decryptSecret($raw_secret, $secret_key);
      if (!$dry_run) {
        if ($this->privateDataCollection) {
          $this->privateDataCollection = NULL;
        }
        $this->real_secret = $real_secret;
        $this->locked = FALSE;
      }
      return TRUE;
    } catch (Exception $e) {
      // Failed to decrypt secret ...
      return FALSE;
    }
  }

  /**
   * Update Coffre fort secret.
   *
   *
   * @param $provider_id
   * @param $secret_key
   * @return mixed|void
   * @throws Exception
   */
  public function updateSecret($provider_id, $secret_key)
  {
    if (!$this->access('edit')) {
      throw new Exception('Trying to update password for locked coffre-fort');
    }

    $encryption_service = $this->getEncryptionService();
    // New password
    if ($this->isNew()) {
      // No secret was create, we need to generate new one.
      $new_secret = $encryption_service->generateNewSecretKey();
      $encrypted_secret = $encryption_service->encrypt($new_secret, $secret_key);
      $this->set('secret', $encrypted_secret);
    } else {
      // Secret is already generated, all you need is to re-encrypt it with the new key.
      $secret = $this->real_secret;
      $encrypted_secret = $encryption_service->encrypt($secret, $secret_key);
      $this->set('secret', $encrypted_secret);
    }
    $this->set('secret_provider', $provider_id);
  }

  /**
   * Check for duplicated items machine names.
   * @param $name
   * @param $element
   * @param $form_state
   * @return bool
   */
  public function privateDataNameExists($name, $element, $form_state)
  {
    foreach ($this->privateDataCollection as $item) {
      if (($item->getName() == $name) && $item->getUuid() != $form_state->getValue("uuid")) {
        return true;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function postLoad(Drupal\Core\Entity\EntityStorageInterface $storage, array &$entities)
  {
    foreach ($entities as $entity) {
      $entity->tryUnlockByProvider();
    }
  }

  /**
   * Return number of items
   * @return int|void
   */
  public function count()
  {
    return count($this->private_data_list ?? []);
  }

  /**
   * Allow save only if unlocked or on create
   * @return int|void
   * @throws Drupal\Core\Entity\EntityStorageException
   */
  public function save()
  {
    if ($this->isNew() or !$this->isLocked()) {
      parent::save();
    }
  }

  /**
   * Clear tokens cache after save.
   * @param EntityStorageInterface $storage
   * @param $update
   * @return void
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE)
  {
    parent::postSave($storage, $update);
    token_clear_cache();
  }
}
