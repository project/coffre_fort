<?php

namespace Drupal\coffre_fort;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Coffre Fort entities.
 */
interface CoffreFortEntityInterface extends ConfigEntityInterface
{


  /**
   * Returns a specific private data .
   *
   * @param string $private_data
   *   The private data ID.
   *
   * @return PrivateDataInterface
   *   The private data object.
   */
  public function getPrivateData($private_data);

  /**
   * Returns all private data in this coffre-fort.
   *
   * @return PrivateDataPluginCollection|PrivateDataInterface[]
   *   The private data plugin collection.
   */
  public function getPrivateDataList();

  /**
   * Saves a private data for this coffre-fort.
   *
   * @param array $configuration
   *   An array of private data configuration.
   *
   * @return string
   *   The private data ID.
   */
  public function addPrivateData(array $configuration);

  /**
   * Deletes an private data from this coffre fort.
   *
   * @param PrivateDataInterface $private_data
   *   The private data object.
   *
   * @return $this
   */
  public function deletePrivateData(PrivateDataInterface $private_data);


  /**
   * set or update current secret key
   * @param $provider_id
   * @param $secret_key
   * @return mixed
   */
  public function updateSecret($provider_id, $secret_key);
}
