<?php

namespace Drupal\coffre_fort\EventSubscriber;

use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Config\StorageTransformEvent;
use Drupal\Core\Site\Settings;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Provides a ConfigSubscriber that exclude coffre_fort config
 */
class ConfigSubscriber implements EventSubscriberInterface
{

  /**
   * @var StorageInterface
   */
  private $activeStorage;

  /**
   * @var ConfigManagerInterface
   */
  private $manager;

  /**
   * EnvironmentModulesEventSubscriber constructor.
   *
   * @param StorageInterface $active_storage
   *   The active config storage.
   * @param Settings $settings
   *   The Drupal settings.
   * @param ConfigManagerInterface $manager
   *   The config manager.
   */
  public function __construct(StorageInterface $active_storage, ConfigManagerInterface $manager)
  {
    $this->activeStorage = $active_storage;
    $this->manager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array
  {
    // React early on export and late on import.
    return [
      'config.transform.import' => ['onConfigTransformImport', -500],
      'config.transform.export' => ['onConfigTransformExport', 500],
    ];
  }

  /**
   * Transform the storage which is used to import the configuration.
   *
   * Make sure excluded modules are not uninstalled by adding them and their
   * config to the storage when importing configuration.
   *
   * @param StorageTransformEvent $event
   *   The transformation event.
   */
  public function onConfigTransformImport(StorageTransformEvent $event)
  {
    $storage = $event->getStorage();
    if (!$storage->exists('core.extension')) {
      // If the core.extension config is not present there is nothing to do.
      // This means that probably the storage is empty or non-functional.
      return;
    }

    foreach (array_merge([StorageInterface::DEFAULT_COLLECTION], $this->activeStorage->getAllCollectionNames()) as $collectionName) {
      $collection = $storage->createCollection($collectionName);
      $activeCollection = $this->activeStorage->createCollection($collectionName);
      foreach ($this->getDependentConfigNames() as $configName) {
        if (!$collection->exists($configName) && $activeCollection->exists($configName)) {
          // Make sure the config is not removed if it exists.
          $collection->write($configName, $activeCollection->read($configName));
        }
      }
    }

    $extension = $storage->read('core.extension');
    $existing = $this->activeStorage->read('core.extension');

    $modules = $extension['module'];
    if (array_key_exists('coffre_fort', $existing['module'])) {
      // Set the modules weight from the active store.
      $modules['coffre_fort'] = $existing['module']['coffre_fort'];
    }

    // Sort the extensions.
    $extension['module'] = module_config_sort($modules);
    // Set the modified extension.
    $storage->write('core.extension', $extension);
  }

  /**
   * Transform the storage which is used to export the configuration.
   *
   * Make sure excluded modules are not exported by removing all the config
   * which depends on them from the storage that is exported.
   *
   * @param StorageTransformEvent $event
   *   The transformation event.
   */
  public function onConfigTransformExport(StorageTransformEvent $event)
  {

    $storage = $event->getStorage();
    if (!$storage->exists('core.extension')) {
      // If the core.extension config is not present there is nothing to do.
      // This means some other process has rendered it non-functional already.
      return;
    }

    foreach (array_merge([StorageInterface::DEFAULT_COLLECTION], $storage->getAllCollectionNames()) as $collectionName) {
      $collection = $storage->createCollection($collectionName);
      foreach ($this->getDependentConfigNames() as $configName) {
        $collection->delete($configName);
      }
    }

    $extension = $storage->read('core.extension');
    // Remove all the excluded modules from the extensions list.
    unset($extension['module']['coffre_fort']);

    $storage->write('core.extension', $extension);
  }


  /**
   * Get all the configuration which depends on one of the excluded modules.
   *
   * @return string[]
   *   An array of configuration names.
   */
  private function getDependentConfigNames()
  {


    $dependencyManager = $this->manager->getConfigDependencyManager();
    $config = [];

    // Find all the configuration depending on the excluded modules.
    foreach ($dependencyManager->getDependentEntities('module', 'coffre_fort') as $dependent) {
      $config[] = $dependent->getConfigDependencyName();
    }
    $config = array_merge($config, $this->activeStorage->listAll('coffre_fort.'));
    // Find all configuration that depends on the configuration found above.
    foreach ($this->manager->findConfigEntityDependencies('config', array_unique($config)) as $dependent) {
      $config[] = $dependent->getConfigDependencyName();
    }

    return array_unique($config);
  }

}
