<?php

namespace Drupal\coffre_fort\Plugin\CoffreFort\SecretProvider;


use Drupal\coffre_fort\UserSecretProviderBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Simple secret encryption privder based on a user provided password.
 *
 * @UserSecretProvider(
 *   id = "keycloak",
 *   label = @Translation("Openid secret provided [Not implemented]"),
 *   description = @Translation("Use openid keycloak to encrypt coffre-fort secret.")
 * )
 */
class KeycloakSecretProvider extends UserSecretProviderBase
{

  public function buildConfigurationForm(array $form, FormStateInterface $form_state)
  {
    /**
     * @todo implement a way to retrieve a unique information , private and available
     * only for current user. or a way to generate a new secret encryption key
     * and store it within a remote service.
     */

    $form['user_secret']['keycloak_details'] = [
      '#type' => 'fieldset',
      '#title' => 'Openid secret provided',
      '#visible' => TRUE,
      '#description' => $this->t('@todo implement a way to retrieve a unique information , private and available
only for current user. or a way to generate a new secret encryption key
and store it within a remote service..'),
      '#attributes' => ['style' => ['width: 900px']],
      '#states' => [
        'visible' => [
          ':input[name="providertype"]' => ['value' => 'keycloak'],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state)
  {
    $form_state->setErrorByName($form['keycloak_details'], $this->t('Openid secret provided is not fully implemented,you need to choose another privder'));
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state)
  {
  }
}
