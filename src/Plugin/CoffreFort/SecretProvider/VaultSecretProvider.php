<?php

namespace Drupal\coffre_fort\Plugin\CoffreFort\SecretProvider;


use Drupal\coffre_fort\UserSecretProviderBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Simple secret encryption privder based on a user provided password.
 *
 * @UserSecretProvider(
 *   id = "vault",
 *   label = @Translation("vault secret provided [Not implemented]"),
 *   description = @Translation("Use vault per user keys to encrypt coffre-fort secret.")
 * )
 */
class VaultSecretProvider extends UserSecretProviderBase
{

  public function buildConfigurationForm(array $form, FormStateInterface $form_state)
  {
    /**
     * @todo implement a way to retrieve a unique information , private and available
     * only for current user. or a way to generate a new secret encryption key
     * and store it within vault.
     */

    $form['user_secret']['vault_details'] = [
      '#type' => 'fieldset',
      '#title' => 'Vault secret provided',
      '#visible' => TRUE,
      '#description' => $this->t('@todo implement a way to retrieve a unique information , private and available
only for current user. or a way to generate a new secret encryption key
and store it within vault..'),
      '#attributes' => ['style' => ['width: 900px']],
      '#states' => [
        'visible' => [
          ':input[name="providertype"]' => ['value' => 'vault'],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state)
  {
    $form_state->setErrorByName($form['vault_details'], $this->t('Vault secret provided is not fully implemented,you need to choose another privder'));
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state)
  {
  }
}
