<?php

namespace Drupal\coffre_fort\Plugin\CoffreFort\SecretProvider;


use Drupal;
use Drupal\coffre_fort\CoffreFortEncryption;
use Drupal\coffre_fort\Entity\CoffreFortEntity;
use Drupal\coffre_fort\UserSecretProviderBase;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Simple secret encryption privder based on a user provided password.
 *
 * @UserSecretProvider(
 *   id = "password",
 *   label = @Translation("User password"),
 *   description = @Translation("Use user provided password to encrypt coffre-fort secret.")
 * )
 */
class PasswordSecretProvider extends UserSecretProviderBase
{
  /**
   * Datetime service
   * @var Drupal\Component\Datetime\TimeInterface
   */
  protected $datatime;
  /**
   * @var CoffreFortEncryption
   */
  protected $encryption;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerInterface $logger, TimeInterface $datetime, CoffreFortEncryption $encryption, Drupal\Core\State\State $state)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger);
    $this->datatime = $datetime;
    $this->encryption = $encryption;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')->get('coffre_fort'),
      $container->get('datetime.time'),
      $container->get('coffre_fort.encryption'),
      $container->get('state')
    );
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state)
  {
    $entity = $form_state->get('entity');
    $form['user_secret']['password_defails'] = [
      '#type' => $entity->isNew() ? 'fieldset' : 'details',
      '#title' => $this->t('Coffre-fort password') . ($entity->isNew() ? '' : $this->t(' - Update')),
      '#visible' => TRUE,
      '#collapsible' => $entity->isNew(),
      '#collapsed' => !$entity->isNew(),
      '#description' => $this->t('User defined password for this coffre fort, the password is never stored.
       it will be used to encrypt all Coffre-fort private data.'),
      '#attributes' => ['style' => ['width: 900px']],
      '#states' => [
        'visible' => [
          ':input[name="providertype"]' => ['value' => 'password'],
        ],
      ]
    ];

    $form['user_secret']['password_defails']['password'] = [
      '#type' => 'password_confirm',
      '#required' => $entity->isNew(),
      '#size' => 25,
    ];

    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state)
  {

  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state)
  {
    // Check for new password
    /** @var CoffreFortEntity $entity */
    $entity = $form_state->getCompleteFormState()->get('entity');
    $secret = $form_state->getValue('user_secret');
    $new_password = $secret['password_defails']['password'];
    if (strlen($new_password)) {
      // Set or replace the secret encryption
      $entity->updateSecret('password', $new_password);

      $name = 'DRUPAL:CFTOKEN:' . $entity->uuid();
      $secret = $entity->getSecret($new_password);
      $secret = $this->encryption->encrypt($secret, $this->getDoubleSecretKey());
      $value = base64_encode($secret);
      $expire = $this->datatime->getCurrentTime() + 60 * 60 * 24; // Expire in 24h
      $response = new RedirectResponse($entity->toUrl('edit-form')->toString());
      $response->headers->setCookie(\Symfony\Component\HttpFoundation\Cookie::create($name, $value, $expire));
      $form_state->setResponse($response);
    }
  }

  public function buildUnlockForm(array $form, FormStateInterface $form_state)
  {
    $entity = $form_state->get('entity');
    $form['user_secret']['password_defails'] = [
      '#type' => 'fieldset',
      '#title' => 'Coffre-fort password',
      '#visible' => TRUE,
      '#required' => TRUE,
      '#description' => $this->t('User defined password for this coffre fort, the password is never stored.
       it will be used to decrypt all Coffre-fort secret private data.'),
      '#attributes' => ['style' => ['width: 900px']],
      '#states' => [
        'visible' => [
          ':input[name="secret_provider"]' => ['value' => 'password'],
        ],
      ],
    ];

    $form['user_secret']['password_defails']['password'] = [
      '#type' => 'password',
      '#required' => TRUE,
      '#size' => 25,
    ];

    return $form;
  }

  public function validateUnclockForm(array $form, FormStateInterface $form_state)
  {
    // Check for password
    /** @var CoffreFortEntity $entity */
    $entity = $form_state->getCompleteFormState()->get('entity');
    $secret = $form_state->getValue('user_secret');
    $password = $secret['password_defails']['password'];
    if (strlen($password)) {
      if (!$entity->unlock($password, true)) {
        $form_state->setError($form['user_secret']['password_defails']['password'], $this->t('Invalid password'));
      }
    } else {
      $form_state->setError($form['user_secret']['password_defails']['password'], $this->t('Empty password'));
    }
  }

  public function submitUnlockForm(array $form, FormStateInterface $form_state)
  {
    /** @var CoffreFortEntity $entity */
    $entity = $form_state->getCompleteFormState()->get('entity');
    $secret = $form_state->getValue('user_secret');
    $password = $secret['password_defails']['password'];
    $entity->unlock($password);
    // Ulocked so we store create a new token as client cookie
    //$form_state->getResponse()->headers->setCookie(new Cookie('name', 'value', 0, '/', NULL, FALSE, TRUE, FALSE, NULL));
    $name = 'DRUPAL:CFTOKEN:' . $entity->uuid();
    $secret = $entity->getSecret($password);
    $secret = $this->encryption->encrypt($secret, $this->getDoubleSecretKey());
    $value = base64_encode($secret);
    $expire = $this->datatime->getCurrentTime() + 60 * 60 * 24; // Expire in 24h
    $response = new RedirectResponse($entity->toUrl('edit-form')->toString());
    $response->headers->setCookie(\Symfony\Component\HttpFoundation\Cookie::create($name, $value, $expire));
    $form_state->setResponse($response);
  }


  public function buildlockForm(array $form, FormStateInterface $form_state)
  {
    $entity = $form_state->get('entity');

    $form['#title'] = t("Do you realy want to relock you coffre-fort ?");

    $form['#attributes']['class'][] = 'confirmation';
    $form['description'] = ['#markup' => $this->t('Your session will not expire, you can unlock any time .')];
    $form['confirm'] = ['#type' => 'hidden', '#value' => 1];
    return $form;
  }

  public function validatelockForm(array $form, FormStateInterface $form_state)
  {

  }

  public function submitlockForm(array $form, FormStateInterface $form_state)
  {
    /** @var CoffreFortEntity $entity */
    $entity = $form_state->getCompleteFormState()->get('entity');
    $name = 'DRUPAL:CFTOKEN:' . $entity->uuid();
    $url = new Url('entity.coffre_fort.collection');
    $response = new RedirectResponse($url->toString());
    $response->headers->setCookie(\Symfony\Component\HttpFoundation\Cookie::create($name, NULL, 1));
    $form_state->setResponse($response);
  }

  public function getSecretKey($entity)
  {
    // Check for cookies
    $request = Drupal::request();
    $name = 'DRUPAL:CFTOKEN:' . $entity->uuid();
    if ($request->cookies->has($name)) {
      $value = $request->cookies->get($name);
      $value = base64_decode($value);
      $value = $this->encryption->decrypt($value, $this->getDoubleSecretKey());
      return $value;
    }
    return FALSE;
  }

  private function getDoubleSecretKey()
  {
    $key = $this->state->get('coffre_fort.password_encryption_key');
    if (empty($key)) {
      $key = $this->encryption->getRandom(55);
      $this->state->set('coffre_fort.password_encryption_key', $key);
    }
    return $key;
  }

}
