<?php

namespace Drupal\coffre_fort\Plugin\CoffreFort\DataTypes;

use Drupal;
use Drupal\coffre_fort\Annotation\PrivateDataType;
use Drupal\coffre_fort\ConfigurablePrivateDataBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\BubbleableMetadata;

/**
 * Rich text as private data.
 *
 * @PrivateDataType(
 *   id = "text",
 *   label = @Translation("Rich text"),
 *   description = @Translation("Store rich html text as private data.")
 * )
 */
class Text extends ConfigurablePrivateDataBase
{
  public function buildConfigurationForm(array $form, FormStateInterface $form_state)
  {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['secret'] = [
      '#type' => 'text_format',
      '#title' => t('Secret'),
      '#default_value' => $this->configuration['secret']['value'],
      '#format' => $this->configuration['secret']['format'] ?? 'full_html'
    ];

    $form['replacement'] = [
      '#type' => 'text_format',
      '#title' => t('Replacement'),
      '#default_value' => $this->configuration['replacement']['value'],
      '#format' => $this->configuration['replacement']['format'] ?? 'full_html'
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration()
  {
    return [
      'secret' => ['format' => NULL, 'value' => ''],
      'replacement' => ['format' => NULL, 'value' => '']
    ];
  }

  public function render($locked, BubbleableMetadata $bubbleable_metadata)
  {
    $build = [
      '#type' => 'processed_text',
      '#text' => $locked ? $this->configuration['replacement']['value'] : $this->configuration['secret']['value'],
      '#format' => $locked ? $this->configuration['replacement']['format'] : $this->configuration['secret'] ['format'],
      '#cache' => $locked ? [] : ['max-age' => 0]
    ];
    return Drupal::service('renderer')->renderPlain($build);
  }
}
