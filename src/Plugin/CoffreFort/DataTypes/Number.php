<?php

namespace Drupal\coffre_fort\Plugin\CoffreFort\DataTypes;


use Drupal\coffre_fort\Annotation\PrivateDataType;
use Drupal\coffre_fort\ConfigurablePrivateDataBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Number as private data.
 *
 * @PrivateDataType(
 *   id = "number",
 *   label = @Translation("Number"),
 *   description = @Translation("Store a number as private data.")
 * )
 */
class Number extends ConfigurablePrivateDataBase
{

  public function buildConfigurationForm(array $form, FormStateInterface $form_state)
  {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['secret'] = [
      '#type' => 'number',
      '#title' => t('Secret'),
      '#default_value' => $this->configuration['secret'],
      '#required' => TRUE,
    ];
    $form['replacement'] = [
      '#type' => 'number',
      '#title' => t('Replacement'),
      '#default_value' => $this->configuration['replacement'],
      '#required' => TRUE,
    ];
    return $form;
  }
}
