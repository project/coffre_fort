<?php

namespace Drupal\coffre_fort\Plugin\CoffreFort\DataTypes;


use Drupal\coffre_fort\Annotation\PrivateDataType;
use Drupal\coffre_fort\ConfigurablePrivateDataBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Simple string as private data.
 *
 * @PrivateDataType(
 *   id = "string",
 *   label = @Translation("Simple string"),
 *   description = @Translation("Store simple string as private data.")
 * )
 */
class SimpleString extends ConfigurablePrivateDataBase
{

  public function buildConfigurationForm(array $form, FormStateInterface $form_state)
  {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['secret'] = [
      '#type' => 'textfield',
      '#title' => t('Secret'),
      '#default_value' => $this->configuration ['secret'],
      '#required' => TRUE,
    ];

    $form['replacement'] = [
      '#type' => 'textfield',
      '#title' => t('Replacement'),
      '#default_value' => $this->configuration['replacement'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state)
  {
    $coffre = $form_state->getBuildInfo()['args'][0];
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['secret'] = $form_state->getValue('secret');
    $this->configuration['encrypted'] = $coffre->encrypt($form_state->getValue('secret'));
    $this->configuration['replacement'] = $form_state->getValue('replacement');
  }
}
