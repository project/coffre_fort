<?php

namespace Drupal\coffre_fort;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines the interface for private data.
 */
interface PrivateDataInterface extends PluginInspectionInterface, ConfigurableInterface, DependentPluginInterface
{


  /**
   * Returns the private data label.
   *
   * @return string
   *   The private data label.
   */
  public function label();

  public function setLabel($label);

  /**
   * Returns the private data type.
   *
   * @return string
   *   The private data type.
   */
  public function type();

  public function getName();

  public function setName($name);

  /**
   * Returns the unique ID representing the private data.
   *
   * @return string
   *   The private data ID.
   */
  public function getUuid();

  /**
   * Returns the weight of the private data.
   *
   * @return int|string
   *   Either the integer weight of the private data, or an empty string.
   */
  public function getWeight();

  /**
   * Sets the weight for this private data.
   *
   * @param int $weight
   *   The weight for this private data.
   *
   * @return $this
   */
  public function setWeight($weight);

}
