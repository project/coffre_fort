<?php

namespace Drupal\coffre_fort;

use Drupal;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Render\BubbleableMetadata;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base class for private data.
 */
abstract class PrivateDataBase extends PluginBase implements PrivateDataInterface, ContainerFactoryPluginInterface
{

  /**
   * The private data ID.
   *
   * @var string
   */
  protected $uuid;

  /**
   * The private data machine name.
   *
   * @var string
   */
  protected $name;

  /**
   * The private data ID.
   *
   * @var string
   */
  protected $label;

  /**
   * The weight of the private data.
   *
   * @var int|string
   */
  protected $weight = '';

  /**
   * A logger instance.
   *
   * @var LoggerInterface
   */
  protected $logger;


  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerInterface $logger)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->setConfiguration($configuration);
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')->get('coffre_fort')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transformDimensions(array &$dimensions, $uri)
  {
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeExtension($extension)
  {
    return $extension;
  }

  /**
   * {@inheritdoc}
   */
  public function label()
  {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function type()
  {
    return $this->pluginDefinition['type'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getUuid()
  {
    return $this->uuid;
  }

  /**
   * {@inheritdoc}
   */
  public function getName()
  {
    return $this->name;
  }


  /**
   * {@inheritdoc}
   */
  public function setWeight($weight)
  {
    $this->weight = $weight;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setLabel($label)
  {
    $this->label = $label;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name)
  {
    $this->name = $name;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight()
  {
    return $this->weight;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration()
  {
    $data = $this->configuration;
    unset($data['secret']);
    return [
      'uuid' => $this->getUuid(),
      'id' => $this->getPluginId(),
      'label' => $this->label(),
      'name' => $this->getName(),
      'weight' => $this->getWeight(),
      'data' => $data
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration)
  {
    $configuration += [
      'data' => [],
      'label' => '',
      'id' => '',
      'name' => '',
      'uuid' => '',
      'weight' => '',
    ];
    $this->configuration = $configuration['data'] + $this->defaultConfiguration();
    $this->uuid = $configuration['uuid'];
    $this->weight = $configuration['weight'];
    $this->label = $configuration['label'];
    $this->name = $configuration['name'];
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration()
  {
    return [
      'secret' => '',
      'replacement' => ''
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies()
  {
    return [];
  }

  public function render($locked, BubbleableMetadata $bubbleable_metadata)
  {
    $build = [
      '#markup' => $locked ? $this->configuration['replacement'] : $this->configuration['secret'],
      '#cache' => $locked ? ['max-age' => 0] : []
    ];
    return Drupal::service('renderer')->renderPlain($build);
  }

}
