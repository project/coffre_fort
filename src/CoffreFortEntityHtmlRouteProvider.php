<?php

namespace Drupal\coffre_fort;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;

/**
 * Provides routes for Coffre Fort entities.
 *
 * @see Drupal\Core\Entity\Routing\AdminHtmlRouteProvider
 * @see Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider
 */
class CoffreFortEntityHtmlRouteProvider extends AdminHtmlRouteProvider
{

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type)
  {
    $collection = parent::getRoutes($entity_type);

    // Provide your custom entity routes here.
    return $collection;
  }

}
