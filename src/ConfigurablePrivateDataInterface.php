<?php

namespace Drupal\coffre_fort;

use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Defines the interface for configurable private data.
 *
 */
interface ConfigurablePrivateDataInterface extends PrivateDataInterface, PluginFormInterface
{

}
