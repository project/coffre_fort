<?php

namespace Drupal\coffre_fort;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a base class for configurable private data.
 */
abstract class ConfigurablePrivateDataBase extends PrivateDataBase implements ConfigurablePrivateDataInterface
{

  public function buildConfigurationForm(array $form, FormStateInterface $form_state)
  {
    $form['secret'] = [
      '#type' => 'textarea',
      '#title' => t('Secret'),
      '#default_value' => $this->configuration['secret'],
      '#required' => TRUE,
    ];
    $form['replacement'] = [
      '#type' => 'textarea',
      '#title' => t('Replacement'),
      '#default_value' => $this->configuration['replacement'],
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state)
  {

  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state)
  {
    $coffre = $form_state->getBuildInfo()['args'][0];
    $this->configuration['secret'] = $form_state->getValue('secret');
    $this->configuration['encrypted'] = $coffre->encrypt($form_state->getValue('secret'));
    $this->configuration['replacement'] = $form_state->getValue('replacement');
  }

  public function getValue($locked)
  {
    if ($locked) {
      return $this->configuration ['secret'];
    }
    return $this->configuration ['replacement'];
  }

}
