<?php

namespace Drupal\coffre_fort;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

class CoffreFortAccessControlHandler extends EntityAccessControlHandler
{

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account)
  {
    switch ($operation) {
      case 'view':
        return parent::checkAccess($entity, $operation, $account);

      case 'unlock':
        return AccessResult::allowedIf($entity->isLocked())->addCacheableDependency($entity)
          ->andIf(parent::checkAccess($entity, $operation, $account));
      case 'translate':
      case 'delete':
      case 'update':
      case 'relock':
        return AccessResult::allowedIf(!$entity->isLocked())->addCacheableDependency($entity)
          ->andIf(parent::checkAccess($entity, $operation, $account));
    }
    return parent::checkAccess($entity, $operation, $account);
  }
}
