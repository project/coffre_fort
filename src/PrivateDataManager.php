<?php

namespace Drupal\coffre_fort;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Traversable;

/**
 * Manages private data plugins.
 *
 */
class PrivateDataManager extends DefaultPluginManager
{

  /**
   * Constructs a new PrivateDataManager.
   *
   * @param Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler)
  {
    parent::__construct('Plugin/CoffreFort/DataTypes', $namespaces, $module_handler, 'Drupal\coffre_fort\PrivateDataInterface', 'Drupal\coffre_fort\Annotation\PrivateDataType');

    $this->alterInfo('coffre_fort_private_info');
    $this->setCacheBackend($cache_backend, 'coffre_fort_private_types_plugins');
  }

}
