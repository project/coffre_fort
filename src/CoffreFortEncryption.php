<?php

namespace Drupal\coffre_fort;


use Drupal\Component\Utility\Crypt;
use Exception;

class CoffreFortEncryption
{

  private $ciphering = "AES-256-CTR";
  private $encryption_iv = "0000000000000000";
  private $options = 0;

  public function __construct()
  {
  }

  public function getRandom($count = 55)
  {
    return Crypt::randomBytesBase64($count);
  }

  /**
   *
   * @return void
   */
  public function generateNewSecretKey()
  {
    return json_encode([$this->encryption_iv, $this->getRandom(55)]);
  }

  /**
   *
   * @return void
   */
  public function decryptSecret(string $string, string $key)
  {
    $json = $this->decrypt($string, $key);
    $data = json_decode($json, TRUE);
    if (!is_array($data) or $data[0] != $this->encryption_iv) {
      throw new Exception('Error decrypting secret');
    }

    return $json;
  }

  /**
   * decrypt a string usinc
   * @param $encrypted_secret
   * @param $new_password
   * @return void
   */
  public function decrypt($string, $key)
  {

    $data = openssl_decrypt($string,
      $this->ciphering,
      $key,
      $this->options,
      $this->encryption_iv);

    return @unserialize($data);
  }

  /**
   * @param $encrypted_secret
   * @param $new_password
   * @return void
   */
  public function encrypt($data, $key)
  {
    $data = serialize($data);
    $iv_length = openssl_cipher_iv_length($this->ciphering);
    return openssl_encrypt($data,
      $this->ciphering,
      $key,
      $this->options,
      $this->encryption_iv
    );


  }

}
